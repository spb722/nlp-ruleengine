# @Filename:    text_classification.py
# @Author:      spb72
# @Time:        14-07-2023 15:19
# @Email:
from transformers import BertTokenizer
from torch import nn
from transformers import BertModel
from torch.optim import Adam
from tqdm import tqdm

import torch
from transformers import AutoTokenizer

# Initialize the tokenizer
tokenizer = AutoTokenizer.from_pretrained("bert-base-cased")

def predict(text, model):
    # Identify the device

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    # Preprocess the text

    input = tokenizer(text, padding='max_length', max_length=512, truncation=True, return_tensors="pt")

    # Get the features from the preprocessed text
    mask = input['attention_mask'].to(device)
    input_id = input['input_ids'].to(device)

    # Move model to correct device
    model = model.to(device)

    # Get the model prediction
    with torch.no_grad():
        model.eval()
        output = model(input_id, mask)

    # The output is a probability distribution over the classes
    # So, we'll get the class with the highest probability
    _, predicted_class = torch.max(output, dim=1)

    # Convert the tensor to a simple integer and return it
    return int(predicted_class.cpu())


labels = {'query': 0, 'promotion': 1}
labels_reverse = {v: k for k, v in labels.items()}

# Create a new column 'predicted_category'



class BertClassifier(nn.Module):
    def __init__(self, dropout=0.5):
        super(BertClassifier, self).__init__()
        self.bert = BertModel.from_pretrained('bert-base-cased')
        self.dropout = nn.Dropout(dropout)
        self.linear = nn.Linear(768, 5)
        self.relu = nn.ReLU()

    def forward(self, input_id, mask):
        _, pooled_output = self.bert(input_ids=input_id, attention_mask=mask, return_dict=False)
        dropout_output = self.dropout(pooled_output)
        linear_output = self.linear(dropout_output)
        final_layer = self.relu(linear_output)
        return final_layer


def load_model(path):
    model = BertClassifier()
    use_cuda = torch.cuda.is_available()
    if use_cuda:
        device = torch.device("cuda" if use_cuda else "cpu")
        model = model.to(device)
        model.load_state_dict(torch.load(path))
    else:
        device = torch.device("cuda" if use_cuda else "cpu")
        model = model.to(device)
        model.load_state_dict(torch.load(path, map_location=torch.device('cpu')))

    return model

