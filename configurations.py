# @Filename:    configurations.py
# @Author:      spb72
# @Time:        14-07-2023 15:19
# @Email:


class Config:
    text_classification_model_path = r"E:\6d\nlp\bert_classifier.pth"
    spacy_training_data = [
    ("and their revenue is equal to zero, or phone they are using a Samsung", {"entities": [(45, 59, "=")]}),
    ("so and their revenue is equal to zero or they are using a samsung phone", {
        "entities": [(13, 20, "total_revenue"), (24, 32, "="), (41, 55, "="), (66, 71, "handset_type"),
                     (58, 65, "samsung")]}),
    ("the users age on the network is less than 90 days, and they are using a handset type 2g handset",
     {"entities": [(10, 28, "age_on_network"), (32, 41, "<"), (55, 69, "=")]}),
    ("and handset equal to 2g handset", {"entities": [(4, 11, "handset_type"), (12, 20, "="), (21, 31, "2g handset")]}),
    ("the users age on the network is less than 90 days", {"entities": [(10, 28, "age_on_network"), (32, 41, "<")]}),
    ("data consumption of more than 100 mb", {"entities": [(0, 16, "data_usage")]}),
    (
    "the age on the network greater than 30 days and active, with data usage greater than zero, along with voice revenue "
    "higher than zero before 30 days, while maintaining zero voice usage in the last 30 days.", {"entities": [(4, 22,
                                                                                                               "age_on_network"),
                                                                                                              (23, 35,
                                                                                                               ">"), (
                                                                                                              61, 71,
                                                                                                              "data_usage"),
                                                                                                              (102, 115,
                                                                                                               "tot_voice_rev_og"),
                                                                                                              (116, 127,
                                                                                                               ">"), (
                                                                                                              172, 183,
                                                                                                              "voice_usage")]}),
    ("they have been inactive in terms of outgoing voice calls, data usage, and sms for a consecutive period of 5 days",
     {"entities": [(36, 56, "outgoing_calls")]}),
    ("the user's average revenue per user (arpu) has experienced a significant decrease due to high decrement",
     {"entities": [(11, 35, "arpu")]}),
    ("the recharge amount greater than fifty", {"entities": [(20, 32, ">"), (4, 12, "rbt_subscription")]}),
    ("top up", {"entities": [(0, 6, "rbt_subscription")]}),
    ("refill", {"entities": [(0, 6, "rbt_subscription")]}),
    ("the users age on the network is less than 90 days, and handset type they are using a 2g handset", {
        "entities": [(10, 28, "age_on_network"), (32, 41, "<"), (85, 95, "2g handset"), (68, 82, "="),
                     (55, 62, "handset_type")]}),
    (
    "data usage is equal to zero mb, with a total average revenue per user (arpu) greater than zero and less than or equal to 100, while voice usage is equal to 100 in the last 30 days",
    {"entities": [(0, 10, "data_usage"), (14, 22, "="), (45, 69, "ARPU"), (77, 89, ">"), (99, 108, "<"),
                  (112, 120, "="), (132, 143, "voice_usage"), (147, 155, "=")]}),
    ("the user's age on the network is less than 90 days, and handset type they are using a 2g handset",
     {"entities": [(69, 83, "=")]}),
    ("the user has been on the network for over 30 days, and their main balance equal to zero",
     {"entities": [(18, 31, "age_on_network"), (61, 73, "main_balance"), (74, 82, "=")]}),
    ("the user's age on the network is less than 90 days, and handset type they are using a 2g handset",
     {"entities": [(56, 63, "handset_type"), (86, 96, "2g handset")]}),
    ("the user's age on the network is less than 90 days, and handset type they are using a 2g handset",
     {"entities": [(86, 96, "2g handset")]}),
    ("handset they are using a smartphone",
     {"entities": [(0, 8, "handset_type"), (8, 22, "="), (25, 35, "smartphone")]}),
    ("data revenue is less than 850 and voice revenue is greater than 260",
     {"entities": [(0, 12, "data_total_revenue_ob_ib"), (34, 47, "tot_voice_rev_og"), (16, 25, "<"), (51, 63, ">")]}),
    ("handset they are using a smartphone", {"entities": [(8, 22, "=")]}),
    ("and their revenue is equal to zero or phone they are using a samsung", {
        "entities": [(10, 17, "total_revenue"), (21, 29, "="), (38, 43, "handset_type"), (44, 58, "="),
                     (61, 68, "samsung")]}),
    ("main balance equal to zero", {"entities": [(13, 21, "=")]}),
    ("main balance equal to zero", {"entities": [(0, 12, "main_balance")]}),
    (
    "data usage is equal to zero mb, with a total average revenue per user (arpu) greater than zero and less than or equal to 100, while voice usage is equal to 100 in the last 30 days.",
    {"entities": [(0, 10, "data_usage"), (14, 22, "="), (39, 69, "arpu"), (77, 89, ">"), (99, 108, "<"),
                  (132, 143, "voice_usage")]}),
    (
    "the user's age on the network is more than 30 days, and they have made a recharge greater than zero in the last 30 days, remaining active on the network.",
    {"entities": [(11, 29, "age_on_network"), (33, 42, ">"), (73, 81, "rbt_subscription"), (82, 94, ">")]}),
    ("the user has been on the network for over 30 days", {"entities": [(18, 32, "age_on_network")]}),
    (
    "the user's main balance is less than 200, indicating a relatively low amount available for usage and transactions",
    {"entities": [(11, 23, "main_balance"), (27, 36, "<")]}),
    ("the user's age on the network is less than 90 days", {"entities": [(11, 29, "age_on_network"), (33, 42, "<")]}),
    (
    "based on the information provided, the user is utilizing a smartphone as their handset, indicating they have access to advanced features and capabilities beyond those offered by a basic 2g handset.",
    {"entities": [(59, 69, "smartphone"), (79, 86, "handset_type"), (186, 196, "2g handset")]}),
    ("customers did not recevied any incoming calls and voice revenue is zero",
     {"entities": [(50, 63, "tot_voice_rev_og"), (31, 45, "incoming_calls")]}),
    (
    "the user's network tenure is relatively new, as their age on the network is less than 90 days, suggesting they recently joined and have limited experience with the network's services and features.",
    {"entities": [(54, 72, "age_on_network"), (76, 85, "<")]}),
    ("the total revenue is less than two thousand", {"entities": [(4, 17, "total_revenue"), (21, 30, "<")]}),
    ("more than 30 days, and a main balance of zero.", {"entities": [(0, 9, ">")]}),
    ("the user has been inactive in terms of outgoing voice calls, data usage and sms for less than 15 days",
     {"entities": [(61, 71, "data_usage"), (76, 79, "sms_usage")]}),
    (
    "who have received calls during the last 30 days but have not made calls during the same period and there revenue is equal to zero or their phone they are using is a samsung",
    {"entities": [(105, 112, "total_revenue"), (116, 124, "="), (139, 144, "handset"), (165, 172, "samsung")]}),
    (
    "the user's age on the network is a mere few weeks, indicating they have recently joined and are still in the early stages of their network experience.",
    {"entities": [(11, 29, "age_on_network")]}),
        ]
