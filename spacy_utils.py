# @Filename:    spacy_utils.py.py
# @Author:      spb72
# @Time:        18-07-2023 12:13
# @Email:
import pandas as pd
import spacy
from spacy.tokens import DocBin
from tqdm import tqdm
import configurations as config
import re
import platform
import subprocess

nlp = spacy.load("en_core_web_lg")


class EntityMarker:
    def __init__(self):
        self.nlp2 = spacy.load(r"./output/model-best")

    def mark_entities(self, text):
        doc = self.nlp2(text)
        entity_set = set()
        entity_text = ""
        conjunctions = ["and", "or", "then"]
        prev_entity = None
        prev_conjunction = None

        for token in doc:
            if token.ent_type_ and token.text[0].isalpha():
                if token.ent_type_ not in entity_set:
                    entity_text += token.ent_type_ + " "
                    entity_set.add(token.ent_type_)
                    prev_entity = token.ent_type_
                    prev_conjunction = None

            elif token.text.lower() in conjunctions:
                prev_conjunction = token.text.lower()
                if prev_entity:
                    entity_text += prev_conjunction + " "

            elif token.is_digit:
                if prev_entity:
                    entity_text += token.text + " "

            # Check if there are no more entities or conjunctions
            elif not token.ent_type_ and token.text.lower() not in conjunctions:
                prev_entity = None

        return entity_text.strip()


def get_model():
    nlp2 = spacy.load(r"./output/model-best")
    return nlp2


def mark_entities(text):
    nlp2 = get_model()
    doc = nlp2(text)
    entity_set = set()
    entity_text = ""
    conjunctions = ["and", "or", "then"]
    prev_entity = None
    prev_conjunction = None

    for token in doc:
        if token.ent_type_ and token.text[0].isalpha():
            if token.ent_type_ not in entity_set:
                entity_text += token.ent_type_ + " "
                entity_set.add(token.ent_type_)
                prev_entity = token.ent_type_
                prev_conjunction = None

        elif token.text.lower() in conjunctions:
            prev_conjunction = token.text.lower()
            if prev_entity:
                entity_text += prev_conjunction + " "

        elif token.is_digit:
            if prev_entity:
                entity_text += token.text + " "

        # Check if there are no more entities or conjunctions
        elif not token.ent_type_ and token.text.lower() not in conjunctions:
            prev_entity = None

    return entity_text.strip()


def get_spacy_doc():
    db = DocBin().from_disk("./train.spacy")
    return db


def training():
    print("starting traing")
    db = DocBin()
    # Convert training data to spaCy-compatible format
    for text, annot in tqdm(config.Config.spacy_training_data):
        doc = nlp.make_doc(text)
        ents = []
        for start, end, label in annot["entities"]:
            span = doc.char_span(start, end, label=label, alignment_mode="contract")
            if span is None:
                print(span)
                print("Skipping entity")
            else:
                ents.append(span)
        doc.ents = ents

        db.add(doc)

    # Save the DocBin object to disk
    print("going to save trainied file")
    db.to_disk("./train.spacy")
    init_command = ["python", "-m", "spacy", "init", "fill-config", "base_config.cfg"]

    # Train the model
    train_command = ["python", "-m", "spacy", "train", "base_config.cfg", "--output", "./output", "--paths.train",
                     "./train.spacy", "--paths.dev", "./train.spacy"]

    if platform.system() == 'Windows':
        init_command.append("./base_config.cfg")
        subprocess.run(init_command, shell=True)
        subprocess.run(train_command, shell=True)
    else:
        init_command.append("./base_config.cfg")
        subprocess.run(init_command)
        subprocess.run(train_command)
    print("done ")


# if __name__ == '__main__':
#     training()
