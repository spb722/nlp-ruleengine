# Import necessary libraries
import streamlit as st
import punctuation as pc
import pandas as pd
import utilities as utils
import configurations as config
import text_classification as tc
import os

import spacy_utils as spacy_utils


# Define process function
def process_text(text1, text2):
    """Process text and return result"""
    result = text1 + ' ' + text2  # Replace this with your actual processing function
    return result


def process_text1(user_text):
    # remove  the punctuations
    text_punctuation_removed = pc.removepunc(user_text)
    text_punctuation_corrected = pc.punctuation_corrector(text_punctuation_removed)
    print(" the corrected punctuation text is  ", text_punctuation_corrected)



    # create a df with the text
    df = pd.DataFrame({'category': [""], 'text': [text_punctuation_corrected]})
    df_split = utils.split_sen_rows(df)
    if not os.path.exists(config.Config.text_classification_model_path):
        os.makedirs(config.Config.text_classification_model_path)
    model = tc.load_model(config.Config.text_classification_model_path)
    labels = {'query': 0, 'promotion': 1}
    labels_reverse = {v: k for k, v in labels.items()}
    df_split['predicted_category'] = df_split['text'].apply(lambda x: labels_reverse[tc.predict(x, model)])
    df_query = df_split[df_split['predicted_category'] == 'query']
    print("the query is ")
    print(df.head())
    if len(df_query) == 0:
        return "no query identified my the model please go to training page and input the query"

    # convert text to int like ten twenty  and all we  convert to numeric
    df_query["text"] = df_query["text"].map(lambda x: utils.text2int(x))
    print("after converting text to int ")
    print(df_query.head())

    marker = spacy_utils.EntityMarker()
    df_query['entity_text'] = df_query['text'].apply(marker.mark_entities)

    return df_query['entity_text'].values[0]


def main_test():
    text = "data revenue is less than 850 and voice revenue is greater than 260 Intervention 1 offers a daily Data bundle to the user. Intervention 2 also provides a daily Data bundle. Intervention 3 presents a monthly Voice bundle. Intervention 4 offers a monthly Voice bundle. These interventions aim to promote the usage of data and voice services with a focus on daily and monthly bundles as part of a promotional offer."
    # re_val = process_text1("The age on the network greater than 30 days and active, with data usage greater than zero, "
    #                        "along with voice revenue higher than zero before 30 days. Now as part of a promotional offer, "
    #                        "users are encouraged to recharge their account, and in return, they will 30% bonus on  subscription "
    #                        "")

    re_val = process_text1(text)


    print(re_val)


def main():
    # Add navigation
    st.sidebar.title('Navigation')
    page = st.sidebar.radio('Go to', ['Home', 'Training Text Classification ', 'Training Spacy  '])

    if page == 'Home':
        # Set up home interface
        st.title('NLP Rule Generator ')
        user_text = st.text_area("Enter the query ", "")
        if st.button('Convert '):
            if user_text != "":
                result = process_text1(user_text)
                st.write(result)
            else:
                st.write("Please enter some text.")
    elif page == 'Training Text Classification ':
        # Set up training interface
        st.title('Training Text Classification ')
        text1 = st.text_area("Enter the query here ", "")
        text2 = st.text_area("Enter the promotions given here ", "")
        if st.button('Submit For Training '):
            if text1 != "" and text2 != "":
                result = process_text(text1, text2)
                st.write(result)
            else:
                st.write("Please enter some text in both boxes.")
    elif page == 'Training Spacy  ':
        # Set up training interface
        st.title('Training Spacy')


# Run main function
if __name__ == '__main__':
    main_test()
