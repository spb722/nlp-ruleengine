# @Filename:    punctuation.py.py
# @Author:      spb72
# @Time:        14-07-2023 15:02
# @Email:

from deepmultilingualpunctuation import PunctuationModel


def punctuation_corrector(x):
    model = PunctuationModel()
    result = model.restore_punctuation(x)
    return result


def removepunc(x):
    punctuation = "!”&’()/:;[]_`{|}'~,."
    return x.translate(str.maketrans('', '', punctuation))
